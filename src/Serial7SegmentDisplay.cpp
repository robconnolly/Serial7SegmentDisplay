/***************************************************************************
 *  Copyright 2013 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Smartclock MK2.                                   *
 *                                                                         *
 *  Smartclock MK2 is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 3 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  Smartclock MK2 is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Smartclock MK2.  If not, see <http://www.gnu.org/licenses/>.*
 ***************************************************************************/

#include "Serial7SegmentDisplay.h"

Serial7SegmentDisplay::Serial7SegmentDisplay(ISerialTransport &transport)
    : transport_(transport)
{
    // clear 7 seg
    clear();
}

void Serial7SegmentDisplay::writeCmd(command_t cmd, uint8_t arg)
{
    #define CMDLEN 2

    uint8_t cmdarr[CMDLEN];
    cmdarr[0] = cmd;
    cmdarr[1] = arg;

    transport_.write(cmdarr, CMDLEN);
}

void Serial7SegmentDisplay::writeCmd(command_t cmd)
{
    transport_.write((uint8_t*)&cmd, sizeof(command_t));
}        

void Serial7SegmentDisplay::write(String msg)
{
    transport_.write((uint8_t*)msg.c_str(), msg.length());
}

void Serial7SegmentDisplay::writeSpecial(special_char_t s)
{
    writeCmd(CMD_SPECIAL, s);
}

void Serial7SegmentDisplay::clear(void)
{
    writeCmd(CMD_CLEAR);
}        

void Serial7SegmentDisplay::brightness(uint8_t b)
{
    writeCmd(CMD_BRIGHTNESS, b);
}

