/***************************************************************************
 *  Copyright 2017 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Serial7SegmentDisplay.                            *
 *                                                                         *
 *  Serial7SegmentDisplay is free software: you can redistribute it and/or *
 *  modify it under the terms of the GNU General Public License as         *
 *  published by the Free Software Foundation, either version 3 of the     *
 *  License, or(at your option) any later version.                         *
 *                                                                         *
 *  Serial7SegmentDisplay is distributed in the hope that it will be       *
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Serial7SegmentDisplay.  If not, see                         *
 *  <http://www.gnu.org/licenses/>.                                        *
 ***************************************************************************/

#ifndef __SERIAL_7SEGMENT_DISPLAY_SPI_H
#define __SERIAL_7SEGMENT_DISPLAY_SPI_H

#include "ISerial7SegmentDisplay.h"
#include "ISerialTransport.h"

class Serial7SegmentDisplay: public ISerial7SegmentDisplay
{
    private:
	ISerialTransport &transport_;

	typedef enum 
	{
	    CMD_CLEAR 		= 0x76,
	    CMD_SPECIAL 	= 0x77,
	    CMD_BRIGHTNESS 	= 0x7A
	} __attribute__((packed)) command_t;

	void writeCmd(command_t cmd, uint8_t arg);
	void writeCmd(command_t cmd);

    public:
        Serial7SegmentDisplay(ISerialTransport &transport);
        void write(String msg);
        void writeSpecial(special_char_t s);
        void clear(void);
        void brightness(uint8_t b);
};

#endif

