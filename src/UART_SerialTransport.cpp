/***************************************************************************
 *  Copyright 2017 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Serial7SegmentDisplay.                            *
 *                                                                         *
 *  Serial7SegmentDisplay is free software: you can redistribute it and/or *
 *  modify it under the terms of the GNU General Public License as         *
 *  published by the Free Software Foundation, either version 3 of the     *
 *  License, or(at your option) any later version.                         *
 *                                                                         *
 *  Serial7SegmentDisplay is distributed in the hope that it will be       *
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Serial7SegmentDisplay.  If not, see                         *
 *  <http://www.gnu.org/licenses/>.                                        *
 ***************************************************************************/

#include "UART_SerialTransport.h"

UART_SerialTransport::UART_SerialTransport(HardwareSerial &uart, uint32_t baud)
    : uart_(uart)
{
    // init UART
    uart_.begin(baud);
}

uint32_t UART_SerialTransport::read(uint8_t *buf, uint32_t buflen)
{
    uint32_t i;
    for(i = 0; i < buflen; i++)
    {
        if(uart_.available() > 0)
	{
	    buf[i] = uart_.read();
	}
    }
    return i;
}

uint32_t UART_SerialTransport::write(uint8_t *buf, uint32_t buflen)
{
    return uart_.write(buf, buflen);
}

UART_SerialTransport::~UART_SerialTransport(void)
{
    uart_.end();
}

