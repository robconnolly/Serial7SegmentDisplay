/***************************************************************************
 *  Copyright 2017 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Serial7SegmentDisplay.                            *
 *                                                                         *
 *  Serial7SegmentDisplay is free software: you can redistribute it and/or *
 *  modify it under the terms of the GNU General Public License as         *
 *  published by the Free Software Foundation, either version 3 of the     *
 *  License, or(at your option) any later version.                         *
 *                                                                         *
 *  Serial7SegmentDisplay is distributed in the hope that it will be       *
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Serial7SegmentDisplay.  If not, see                         *
 *  <http://www.gnu.org/licenses/>.                                        *
 ***************************************************************************/

#include "SPI_SerialTransport.h"

SPI_SerialTransport::SPI_SerialTransport(SPIClass &spi, SPISettings &settings, int ss)
    : spi_(spi), ss_(ss)
{
    // initialize the digital pin as an output.
    pinMode(ss_, OUTPUT);
    digitalWrite(ss_, HIGH);
    // set up SPI bus
    spi_.beginTransaction(settings);
}

uint32_t SPI_SerialTransport::read(uint8_t *buf, uint32_t buflen)
{
    digitalWrite(ss_, LOW);
    for(uint32_t i = 0; i < buflen; i++)
    {
        buf[i] = spi_.transfer(0xFF);
    }
    digitalWrite(ss_, HIGH);
    return buflen;
}

uint32_t SPI_SerialTransport::write(uint8_t *buf, uint32_t buflen)
{
    digitalWrite(ss_, LOW);
    spi_.writeBytes(buf, buflen);
    digitalWrite(ss_, HIGH);
    return buflen;
}

SPI_SerialTransport::~SPI_SerialTransport(void)
{
    digitalWrite(ss_, HIGH);
    spi_.endTransaction();
}

