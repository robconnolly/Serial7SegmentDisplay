/***************************************************************************
 *  Copyright 2017 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Serial7SegmentDisplay.                            *
 *                                                                         *
 *  Serial7SegmentDisplay is free software: you can redistribute it and/or *
 *  modify it under the terms of the GNU General Public License as         *
 *  published by the Free Software Foundation, either version 3 of the     *
 *  License, or(at your option) any later version.                         *
 *                                                                         *
 *  Serial7SegmentDisplay is distributed in the hope that it will be       *
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Serial7SegmentDisplay.  If not, see                         *
 *  <http://www.gnu.org/licenses/>.                                        *
 ***************************************************************************/

#ifndef __I_SPI_SERIAL_TRANSPORT_H
#define __I_SPI_SERIAL_TRANSPORT_H

#include "ISerialTransport.h"
#include <SPI.h>

class SPI_SerialTransport : public ISerialTransport
{
  private:
    SPIClass &spi_;
    int ss_;

  public:
    SPI_SerialTransport(SPIClass &spi, SPISettings &settings, int ss);
    uint32_t read(uint8_t *buf, uint32_t buflen);
    uint32_t write(uint8_t *buf, uint32_t buflen);
    ~SPI_SerialTransport(void);
};

#endif

