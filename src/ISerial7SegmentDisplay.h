/***************************************************************************
 *  Copyright 2017 Rob Connolly <rob@webworxshop.com                       *
 *                                                                         *
 *  This file is part of Serial7SegmentDisplay.                            *
 *                                                                         *
 *  Serial7SegmentDisplay is free software: you can redistribute it and/or *
 *  modify it under the terms of the GNU General Public License as         *
 *  published by the Free Software Foundation, either version 3 of the     *
 *  License, or(at your option) any later version.                         *
 *                                                                         *
 *  Serial7SegmentDisplay is distributed in the hope that it will be       *
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with Serial7SegmentDisplay.  If not, see                         *
 *  <http://www.gnu.org/licenses/>.                                        *
 ***************************************************************************/

#ifndef __I_SERIAL_7SEGMENT_DISPLAY_H
#define __I_SERIAL_7SEGMENT_DISPLAY_H

#include <Arduino.h>

typedef enum
{
    ALLOFF     = 0x00,
    DECIMAL0   = 0x01,
    DECIMAL1   = 0x02,
    DECIMAL2   = 0x04,
    DECIMAL3   = 0x08,
    COLON      = 0x10,
    APOSTROPHE = 0x20
} __attribute__((packed)) special_char_t;

class ISerial7SegmentDisplay
{
    public:
        virtual void write(String msg) = 0;
        virtual void writeSpecial(special_char_t s) = 0;
        virtual void clear(void) = 0;
        virtual void brightness(uint8_t b) = 0;
};

#endif

